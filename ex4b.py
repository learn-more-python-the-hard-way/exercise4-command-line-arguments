#!/usr/bin/env python3
'''Solution using argparse.'''

import argparse


def parse_args():
    """Getting help with --help or -h."""

    parser = argparse.ArgumentParser(description="Here's help")
    # At least three arguments that are flags,
    # meaning they don't take an extra argument
    # but simply putting them on the command line turns something on.
    parser.add_argument("--alpha", action="store_true", help="alpha help")
    parser.add_argument("--beta", action="store_true", help="beta help")
    parser.add_argument("--gamma", action="store_true", help="gamma help")
    # At least three arguments that are options,
    #  meaning they do take an argument
    # and set a variable in your script to that option.
    parser.add_argument("--delta", help="delta help")
    parser.add_argument("--epsilon", help="epsilon help")
    parser.add_argument("--zeta", help="zeta help")
    # Additional "positional" arguments,
    # which are a list of files at the end of all the -- style arguments
    # and can handle Terminal wildcards like */.txt.
    parser.add_argument("rest", nargs="*", help="remaining args")

    return parser.parse_args()


def main():
    """The big tent."""
    print("hello, world")
    args = parse_args()
    print(f"parsed args is {args}")


if __name__ == "__main__":
    main()
