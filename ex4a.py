#!/usr/bin/env python3

import sys


def main():
	print(sys.argv)
	# Getting help with --help or -h.
	if sys.argv[1] == "--help" or sys.argv[1] == "-h":
		print("Here's help.")
	# At least three arguments that are flags,
	# meaning they don't take an extra argument but simply putting them on the command line turns something on.
	flags = ["-a", "-b", "-c"]
	last_flag = 0
	for flag in flags:
		if flag in sys.argv:
			has_flags = True
			flagname = flag.lstrip("-")
			print(f"flag {flagname} is on")
			last_flag = max(last_flag,sys.argv.index(flag))
			
	# At least three arguments that are options, meaning they do take an argument and set a variable in your script to that option.
	script_vars = {}
	opts = ["-d", "-e", "--f"]
	last_opt = 0
	for opt in opts:
		if opt in sys.argv:
			value = sys.argv.index(opt)+1
			varname = opt.lstrip("-")
			script_vars[varname] = sys.argv[value]
			last_opt = max(last_opt, sys.argv.index(opt) + 1)
	print(f"options are {script_vars}")
	# Additional "positional" arguments, which are a list of files at the end of all the -- style arguments
	# and can handle Terminal wildcards like */.txt.
	print(f"last_flag is {last_flag}, last_opt is {last_opt}")
		

			

if __name__ == "__main__":
	main()

